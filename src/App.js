import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
//import Mosca from './componentes/Mosca';
import Contador from './componentes/Contador';
import Calculadora from './componentes/Calculadora';
import Teclado from './componentes/Teclado';

class App extends Component {
  render() {
    return (
      <React.Fragment>
      <h1>Hola mundo</h1>
      {/*<Mosca/>*/}
      <Contador valorInicial="5"/>
      <Teclado/>
      <Calculadora/>
      </React.Fragment>
    );
  }
}

export default App;
