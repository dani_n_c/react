import React, { Component } from 'react';
import './css/Calculadora.css';
import Display from './Display';
import Fila from './Fila';




class Calculadora extends Component {

    constructor(props){
        super(props);
        this.state = {
            display: "",
            vaciarDisplay:true
        }
    
    this.pulsarTecla = this.pulsarTecla.bind(this);

    }

    pulsarTecla(tecla){
        if (tecla=== "C"){
            this.setState({
                vaciarDisplay:true,
                display: ""
            })
           
        }
      
        else if (tecla==="="){
            let total = 0;
            
            let sumands = this.state.display.split("+");
        
            if (sumands.length>1){
                sumands.forEach(sumand => total+=sumand*1);
                this.setState({display: total});
            }else{
                this.setState({display: ""});
            }
            
        } else {
            this.setState({
                display: this.state.display+tecla
            })
        } 
    }
        


    render() {
    return (
        <React.Fragment>
            <h1>Calculadora</h1>
            <div class="calculadora">
                <Display texto={this.state.display} /> 
                <Fila teclas="C" onClick={this.pulsarTecla} />
                <Fila teclas="1,2,3" onClick={this.pulsarTecla} />
                <Fila teclas="4,5,6" onClick={this.pulsarTecla} />
                <Fila teclas="7,8,9" onClick={this.pulsarTecla} />
                <Fila teclas="+,0,=" onClick={this.pulsarTecla} />
                <Fila teclas="/,*,-" onClick={this.pulsarTecla} />
            </div>
            
        </React.Fragment>
    );
  }
}

export default Calculadora;
