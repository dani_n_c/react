import React from 'react';
import Tecla from './Tecla'
export default function Fila(props){

    let teclas = props.teclas.split(",");
    teclas = teclas.map(tecla => <Tecla texto={tecla} onClick={props.onClick} />);


    return (
        <div>
            {teclas}
        </div>
    )
}


